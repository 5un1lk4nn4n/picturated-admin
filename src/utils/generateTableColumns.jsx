import { Tag, Typography } from 'antd';
import React from 'react';

const tagColumns = ['Active', 'Upcoming', 'Completed'];

const colorCodes = ['#89E234', '#FFFF8F', '#D3D3D3'];

const generateColumns = (
  listOfLabels,
  tagTypes = ['sharing_status', 'status']
) => {
  return listOfLabels.map((label) => {
    return {
      key: label,
      title: label.replace('_', '').toUpperCase(),
      dataIndex: label,
      render: (text) => {
        if (tagTypes.includes(label)) {
          console.log({ label, tagColumns });
          const index = tagColumns.indexOf(text);
          console.log({ index });
          return (
            <Tag color={colorCodes[index]} style={{ color: '#4c4c4c' }}>
              {text}
            </Tag>
          );
        }

        return <Typography.Text>{text}</Typography.Text>;
      },
    };
  });
};

export default generateColumns;
