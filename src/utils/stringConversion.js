const camelToSnakeCase = str => str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);

const snakeToCamel = str =>
    str.toLowerCase().replace(/([-_][a-z])/g, group =>
        group
            .toUpperCase()
            .replace('-', '')
            .replace('_', '')
    );

const jsToPythonRequestFormat = (jsFormat) => {
    return Object.keys(jsFormat).reduce((acc, key) => {
        return {...acc, [camelToSnakeCase(key)]: jsFormat[key]};
    }, {})
}

const pythonToJsResponseFormat = (pythonFormat) => {
    return Object.keys(pythonFormat).reduce((acc, key) => {
        return {...acc, [snakeToCamel(key)]: pythonFormat[key]};
    }, {})
}

export {camelToSnakeCase, snakeToCamel, jsToPythonRequestFormat, pythonToJsResponseFormat}