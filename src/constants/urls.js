export const CURRENT_DOMAIN = window.location.origin;
export const BACKEND_DOMAIN = 'https:backend.qolphin.com/api';
export const FACE_DOMAIN = 'https:face.qolphin.com/api';

export const API_URL = BACKEND_DOMAIN;

export const URLS = {
  login: '/token/',
  eventsList: '/events',
  eventsMenu: '/events/select-menu',
  uploader: '/file-manager/',
};
