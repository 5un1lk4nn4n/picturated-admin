import { ConfigProvider } from 'antd';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Routes from './router/index.jsx';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ConfigProvider
      theme={{
        token: {
          // Seed Token
          colorPrimary: '#ba68c9',
          borderRadius: 10,
          fontFamily: 'Inter',

          // Alias Token
          // colorBgContainer: '#f5906e',
        },
      }}
    >
      <Routes />
    </ConfigProvider>
  </React.StrictMode>
);
