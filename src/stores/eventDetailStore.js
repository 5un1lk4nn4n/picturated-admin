import {create} from 'zustand';
import {createJSONStorage, persist} from 'zustand/middleware'

const useEventDetailStore = create(persist((set,) => ({
    eventMenu: [],
    selectedEvent: null,
    dashboard: {
        totalImages: 0,
        totalSharingRequest: 0,
        storage: 0,
        views: 0,
        downloads: 0,
        whatsappCredit: 0,
    },
    event: null,
    createNewEvent: false,
    eventLoading: {
        dashboard: false,
        gallery: false,
        qrTemplate: false,
        preference: false,
    },
    initEventLoading: () => set({
        eventLoading: {
            dashboard: true,
            gallery: true,
            qrTemplate: true,
            preference: true,
        }
    }),
    setCreateNewEvent: (boolVal) => set({createNewEvent: boolVal}),
    setSelectedEvent: (selected) => set({selectedEvent: selected}),
    updateEventLoading: (key, boolVal) => {

        return set(state => {
            const eventLoading = state.eventLoading
            eventLoading[key] = boolVal
            return eventLoading
        })
    },
    setEvent: (event) => set({event: event}),
    setEventMenu: (options, defaultEvent) =>
        set({eventMenu: options, selectedEvent: defaultEvent}),
    setDashboard: (data) => set({dashboard: data}),
}), {
    name: 'eventDetails',
    storage: createJSONStorage(() => sessionStorage)
}));

export default useEventDetailStore;
