import { create } from 'zustand';

const useEventStore = create((set) => ({
  eventTable: [],
  pageNo: 1,
  totalRecords: 0,
  createNewEvent: false,
  eventTableLoading: false,
  searchQuery: null,
  activeSharingStatus: 'All',
  activeStatus: 'All',
  displayCalenderView: false,
  setSearchQuery: (search) => set({ searchQuery: search }),
  setPageNo: (page) => set({ pageNo: page }),
  setEventTable: (data, count) =>
    set({ eventTable: data, totalRecords: count }),
  setActiveSharingStatus: (status) => set({ activeSharingStatus: status }),
  setActiveStatus: (status) => set({ activeStatus: status }),

  toggleDisplayCalenderView: () =>
    set((state) => ({ displayCalenderView: !state.displayCalenderView })),
  toggleLoading: (boolVal) => set(() => ({ eventTableLoading: boolVal })),
}));

export default useEventStore;
