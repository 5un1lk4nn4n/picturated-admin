import jwt_decode from 'jwt-decode';
import PropTypes from 'prop-types';
// import React from 'react';
import { Redirect, Route } from 'wouter';
function ProtectedRoute({ component: Component, ...restOfProps }) {
  try {
    const token = localStorage.getItem('token');
    jwt_decode(token);
    return <Route {...restOfProps} component={Component} />;
    // valid token format
  } catch (error) {
    return <Redirect to="/login" />;
  }
}

export default ProtectedRoute;

ProtectedRoute.propTypes = {
  component: PropTypes.func.isRequired,
};
