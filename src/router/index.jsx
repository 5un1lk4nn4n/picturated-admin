import { Route, Switch } from 'wouter';
import PageNotFound from '../components/PageNotFound';
import EventOverview from '../pages/EventOverview';
import Events from '../pages/Events';
import Login from '../pages/Login';
import QolphinSetting from '../pages/QolphinSetting';
import ProtectedRoute from './Protected';
const Routes = () => (
  <Switch>
    <Route path="/login" component={Login} />
    <ProtectedRoute path="/events-overview" component={EventOverview} />
    <ProtectedRoute path="/qolphin-settings" component={QolphinSetting} />

    <ProtectedRoute path="/events/k" component={Events} />
    <Route path="/qr/:qrcode">
      {(params) => <Events qrCode={params.qrcode} />}
    </Route>
    <ProtectedRoute path="/" component={Events} />
    <Route>
      <PageNotFound />
    </Route>
  </Switch>
);

export default Routes;
