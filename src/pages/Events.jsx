import {PlusCircleOutlined, QuestionCircleOutlined} from '@ant-design/icons';
import {Button, Drawer, Select, Tabs, Typography} from 'antd';
import React, {useState} from 'react';
import Dashboard from '../components/Dashboard';
import Gallery from '../components/Gallery';
import QrCodeDesign from '../components/QrCode';
import Settings from '../components/Settings';
import Support from '../components/Support';
import Topbar from '../components/TopBar';
import EventService from '../service/EventService';
import useEventDetailStore from '../stores/eventDetailStore';
import Spinner from "../utils/Spinner.jsx";

const eventService = new EventService();
const {Title} = Typography;

const defaultOptions = [
    {
        value: '1',
        label: (
            <span style={{paddingLeft: 40}}>
        <PlusCircleOutlined/> Create Event
      </span>
        ),
    },
    {
        label: 'Active',
        options: [
            {
                value: 2,
                label: '(Empty)',
                disabled: true,
            },
        ],
    },
    {
        label: 'Upcoming',
        options: [
            {
                value: 3,
                label: '(Empty)',
                disabled: true,
            },
        ],
    },
];

export default function Events() {
    const [open, setOpen] = useState(false);
    const [activeTab, setActiveTab] = useState('1');
    const eventMenu = useEventDetailStore((state) => state.eventMenu);
    const dashboard = useEventDetailStore((state) => state.dashboard);
    const setEventMenu = useEventDetailStore((state) => state.setEventMenu);
    const selectedEvent = useEventDetailStore((state) => state.selectedEvent);
    const setEvent = useEventDetailStore((state) => state.setEvent);
    const event = useEventDetailStore((state) => state.event);
    const setSelectedEvent = useEventDetailStore(
        (state) => state.setSelectedEvent
    );
    const eventLoading = useEventDetailStore((state) => state.eventLoading);
    const updateEventLoading = useEventDetailStore((state) => state.updateEventLoading);
    const initEventLoading = useEventDetailStore((state) => state.initEventLoading);
    // console.log({
    //     eventLoading
    // })
    React.useEffect(() => {
        var defaultSelected = null;
        eventService.getMenu().then((response) => {
            if (response.active_events.length) {
                defaultOptions[1].options = response.active_events;
                defaultSelected = defaultOptions[1].options[0].value;
            }
            if (response.upcoming_events.length) {
                defaultOptions[2].options = response.upcoming_events;
            }

            setEventMenu(defaultOptions, defaultSelected);
        });
    }, []);

    React.useEffect(() => {
        initEventLoading()
        console.log("React.useEffect", selectedEvent)
        selectedEvent &&
        eventService.getEventDetail(selectedEvent).then((response) => {
            setEvent(response);
            updateEventLoading('preference', false)
        });
    }, [selectedEvent]);

    const tabItems = [
        {
            key: '1',
            label: <>Dashboard {eventLoading.dashboard && <Spinner/>}</>,
            children: <Dashboard data={dashboard}/>,
        },
        {
            key: '2',
            label: <>Gallery {eventLoading.gallery && <Spinner/>}</>,
            children: <Gallery/>,
        },
        {
            key: '3',
            label: <>QR Code Template {eventLoading.qrTemplate && <Spinner/>}</>,
            children: <div><QrCodeDesign/></div>,
        },
        {
            key: '4',
            label: <>Preference {eventLoading.preference && <Spinner/>}</>,
            children: (
                <div style={{width: '60%'}}>
                    <Settings onCreateSubmit={(data) => {
                        setEvent(data);
                        const eventMenus = eventMenu.map((r) => {
                            if (r.value === data.id) {
                                r.label = data.name
                            }
                            return r
                        })
                        setEventMenu(eventMenus, selectedEvent)
                    }} eventData={event}/>
                </div>
            ),
        },
    ];
    const createNewEvent = useEventDetailStore((state) => state.createNewEvent);
    const setCreateNewEvent = useEventDetailStore(
        (state) => state.setCreateNewEvent
    );

    const onSelectedEvent = (value) => {
        console.log(`selected ${value}`);
        if (value === '1') {
            console.log(`selected ${value}`, 'ye');

            setCreateNewEvent(true);
        } else {
            setSelectedEvent(value);
        }
    };

    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setCreateNewEvent(false);
    };
    return (
        <div
            style={{
                width: '100%',
            }}
        >
            <Topbar/>
            <div
                style={{
                    display: 'flex',
                    width: '98%',
                    minHeight: '85vh',
                    border: '1px solid #e7e7ea',
                    padding: 10,
                    borderTop: 'none',
                    flexDirection: 'column',
                }}
            >
                <div
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                >
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'start',
                            gap: 10,
                        }}
                    >
                        <Select
                            placeholder="Choose Event"
                            style={{width: 250}}
                            // size="large"
                            onChange={onSelectedEvent}
                            value={selectedEvent}
                            options={eventMenu}
                        />
                        <Title level={3} style={{margin: 0}}>
                            {event?.name}
                        </Title>
                    </div>

                    <div
                        style={{
                            display: 'flex',
                            gap: 10,
                        }}
                    >
                        <Button icon={<QuestionCircleOutlined/>} onClick={showDrawer}/>
                    </div>
                </div>
                <Tabs
                    onChange={(key) => {
                        setActiveTab(key);
                    }}
                    defaultActiveKey={activeTab}
                    items={tabItems}

                >

                </Tabs>
            </div>
            <Drawer
                title="Help and Support"
                width={720}
                onClose={onClose}
                open={open}
                styles={{
                    body: {
                        paddingBottom: 80,
                    },
                }}
            >
                <Support/>
            </Drawer>
            {createNewEvent && (
                <Drawer
                    title="Create New Event"
                    width={720}
                    onClose={onClose}
                    open={createNewEvent}
                    styles={{
                        body: {
                            paddingBottom: 80,
                        },
                    }}
                >
                    <Settings
                        onCreateSubmit={(data) => {
                            console.log({
                                data,
                            });
                            setEvent(data);
                            const newEventMenu = {
                                label: data.name,
                                value: data.id
                            }

                            const menus = [newEventMenu, ...eventMenu]

                            setEventMenu(menus, data.id)
                        }}
                    />
                </Drawer>
            )}
        </div>
    );
}
