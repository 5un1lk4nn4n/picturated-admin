import { AppstoreOutlined, MailOutlined } from '@ant-design/icons';
import { Menu, Result } from 'antd';
import React from 'react';
import ChangePassword from '../components/ChangePassword';
import CompanyDetail from '../components/CompanyDetails';
import Price from '../components/Price';
import Topbar from '../components/TopBar';
function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const items = [
  getItem('Company', 'company', <MailOutlined />, [
    getItem(
      'Details',
      'detail'
      // null
      // [getItem('Option 1', '1'), getItem('Option 2', '2')],
      // 'group'
    ),
    getItem(
      'Change Password',
      'password'
      // null
      // [getItem('Option 1', '1'), getItem('Option 2', '2')],
      // 'group'
    ),
  ]),
  getItem('Qolphin', 'qolphin', <AppstoreOutlined />, [
    getItem('Plan', 'plan'),
    getItem('Help & Support', 'help'),
    // getItem('Submenu', 'sub3', null, [
    //   getItem('Option 7', '7'),
    //   getItem('Option 8', '8'),
    // ]),
  ]),
  // {
  //   type: 'divider',
  // },
  // getItem('Navigation Three', 'sub4', <SettingOutlined />, [
  //   getItem('Option 9', '9'),
  //   getItem('Option 10', '10'),
  //   getItem('Option 11', '11'),
  //   getItem('Option 12', '12'),
  // ]),
  // getItem(
  //   'Group',
  //   'grp',
  //   null,
  //   [getItem('Option 13', '13'), getItem('Option 14', '14')],
  //   'group'
  // ),
];

export default function QolphinSetting() {
  const [selected, setSelected] = React.useState('detail');
  const onClick = (e) => {
    console.log('click ', e);
    setSelected(e.key);
  };

  const getComponents = () => {
    switch (selected) {
      case 'detail':
        return <CompanyDetail />;
      case 'password':
        return <ChangePassword />;
      case 'plan':
        return (
          <div
            style={{
              marginTop: 50,
            }}
          >
            <Price />
          </div>
        );
      default:
        return (
          <Result
            title="Under development"
            style={{
              paddingTop: '20%',
              height: '80vh',
            }}
          />
        );
    }
  };
  return (
    <div
      style={{
        width: '100%',
      }}
    >
      <Topbar />
      <div
        style={{
          display: 'flex',
        }}
      >
        <div style={{}}>
          <Menu
            onClick={onClick}
            style={{
              width: 256,
            }}
            defaultSelectedKeys={['detail']}
            defaultOpenKeys={['company', 'qolphin']}
            mode="inline"
            items={items}
          />
        </div>
        <div
          style={{
            width: '100%',
            marginLeft: 20,
          }}
        >
          {getComponents()}
        </div>
      </div>
    </div>
  );
}
