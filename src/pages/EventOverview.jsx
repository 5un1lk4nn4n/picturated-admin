import { CalendarOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { Button, Input, Segmented, Table, Tag, Typography } from 'antd';
import dayjs from 'dayjs';
import React from 'react';
import CalenderView from '../components/CalenderView';
import Topbar from '../components/TopBar';
import EventService from '../service/EventService';
import useEventStore from '../stores/eventListStore';
const { Search } = Input;

const tagColumns = ['Active', 'Upcoming', 'Completed'];
const colorCodes = ['#89E234', '#FFFF8F', '#D3D3D3'];
const eventService = new EventService();

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Event Code',
    dataIndex: 'qr_code',
    render: (text) => {
      return <Typography.Text>{text}</Typography.Text>;
    },
  },
  {
    title: 'Start Date',
    dataIndex: 'start_date',
    render: (date) => {
      return (
        <Typography.Text>{dayjs(date).format('DD-MMM-YYYY')}</Typography.Text>
      );
    },
  },
  {
    title: 'End Date',
    dataIndex: 'end_date',
    render: (date) => {
      return (
        <Typography.Text>{dayjs(date).format('DD-MMM-YYYY')}</Typography.Text>
      );
    },
  },
  {
    title: 'Photo sharing expiry date',
    dataIndex: 'photo_sharing_expiry_date',
    render: (date) => {
      return (
        <Typography.Text>{dayjs(date).format('DD-MMM-YYYY')}</Typography.Text>
      );
    },
  },
  // {
  //   title: 'Sharing Expire in (days)',
  //   dataIndex: 'photo_sharing_expiry_days',
  // },

  {
    title: 'Event Status',
    dataIndex: 'status',
    render: (status) => {
      const index = tagColumns.indexOf(status);
      return (
        <Tag color={colorCodes[index]} style={{ color: '#4c4c4c' }}>
          {status}
        </Tag>
      );
    },
  },
  {
    title: 'Sharing Status',
    dataIndex: 'sharing_status',
    render: (status) => {
      const index = tagColumns.indexOf(status);
      return (
        <Tag color={colorCodes[index]} style={{ color: '#4c4c4c' }}>
          {status}
        </Tag>
      );
    },
  },
];

export default function EventOverview() {
  const eventTable = useEventStore((state) => state.eventTable);
  const pageNo = useEventStore((state) => state.pageNo);
  const setPageNo = useEventStore((state) => state.setPageNo);
  const displayCalenderView = useEventStore(
    (state) => state.displayCalenderView
  );
  const toggleDisplayCalenderView = useEventStore(
    (state) => state.toggleDisplayCalenderView
  );
  const totalRecords = useEventStore((state) => state.totalRecords);
  const activeSharingStatus = useEventStore(
    (state) => state.activeSharingStatus
  );
  const activeStatus = useEventStore((state) => state.activeStatus);
  const searchQuery = useEventStore((state) => state.searchQuery);
  const setSearchQuery = useEventStore((state) => state.setSearchQuery);
  const setActiveSharingStatus = useEventStore(
    (state) => state.setActiveSharingStatus
  );
  const setActiveStatus = useEventStore((state) => state.setActiveStatus);
  const setEventTable = useEventStore((state) => state.setEventTable);
  const eventTableLoading = useEventStore((state) => state.eventTableLoading);
  const toggleLoading = useEventStore((state) => state.toggleLoading);

  const onSearch = (value) => {
    fetchEventTable({
      pageNo: pageNo,
      status: activeStatus,
      sharingStatus: activeSharingStatus,
      ...(value && { search: value }),
    });
  };

  React.useEffect(() => {
    fetchEventTable({ pageNo: 1 });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    fetchEventTable({
      pageNo: pageNo,
      status: activeStatus,
      sharingStatus: activeSharingStatus,
      ...(searchQuery && { search: searchQuery }),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageNo, activeStatus, activeSharingStatus]);

  const fetchEventTable = ({ pageNo, status, sharingStatus, search }) => {
    toggleLoading(true);
    eventService
      .getEventList({
        page: pageNo,
        status,
        sharing: sharingStatus,
        ...(search && { search: search }),
      })
      .then((res) => {
        const { count, results } = res;
        setEventTable(results, count);
      })
      .finally(() => toggleLoading(false));
  };

  return (
    <div
      style={{
        width: '100%',
      }}
    >
      <Topbar />
      <div
        style={{
          padding: 20,
        }}
      >
        <div
          style={{
            display: 'flex',
            gap: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Search
            placeholder="Search Events by name or code"
            // allowClear
            enterButton="Search"
            onSearch={onSearch}
            style={{
              width: 350,
            }}
            onChange={(e) => {
              setSearchQuery(e.target.value);
            }}
            value={searchQuery}
          />

          <div
            style={{
              gap: 10,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Typography.Text type="secondary">Event Status</Typography.Text>
            <Segmented
              options={['All', 'Active', 'Upcoming', 'Completed']}
              onChange={(value) => {
                setActiveStatus(value);
              }}
            />
          </div>
          <div
            style={{
              gap: 10,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Typography.Text type="secondary">Sharing Status</Typography.Text>
            <Segmented
              options={['All', 'Active', 'Upcoming', 'Completed']}
              onChange={(value) => {
                setActiveSharingStatus(value);
              }}
            />
          </div>
          <div
            style={{
              gap: 10,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Button
              onClick={() => {
                toggleDisplayCalenderView();
              }}
              icon={
                displayCalenderView ? (
                  <CalendarOutlined />
                ) : (
                  <UnorderedListOutlined />
                )
              }
            />
          </div>
        </div>
        {displayCalenderView ? (
          <CalenderView />
        ) : (
          <Table
            loading={eventTableLoading}
            bordered
            pagination={{
              total: totalRecords,
              onChange: (currentPage) => {
                setPageNo(currentPage);
              },
            }}
            columns={columns}
            dataSource={eventTable}
            size="small"
            style={{
              marginTop: 20,
            }}
          />
        )}
      </div>
    </div>
  );
}
