import { LockOutlined, MobileOutlined } from '@ant-design/icons';
import { Alert, Button, Form, Input, Space, Typography, theme } from 'antd';
import React from 'react';
import rangoli from '../assets/rangoli.svg';
import LoginService from '../service/auth/LoginService';
const { useToken } = theme;
const loginService = new LoginService();
const Login = () => {
  const [errorMessage, setErrorMessage] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const { token } = useToken();
  const onFinish = (credentials) => {
    setLoading(true);
    loginService.login(credentials).catch((error) => {
      console.log('error', error);
      setErrorMessage(error.message);
      setLoading(false);
    });
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center',
        height: '97vh ',
        flexDirection: 'row',
      }}
    >
      <div
        style={{
          display: 'flex',
          width: '40%',
          backgroundColor: token.colorPrimary,
          height: '97vh',
          justifyContent: 'center',
          alignItems: 'center',
          color: 'white',
          flexDirection: 'column',
        }}
      >
        <LockOutlined style={{ fontSize: '4em' }} />
        <Typography.Title level={1} style={{ fontSize: '3em', color: '#fff' }}>
          QOLPHIN
        </Typography.Title>

        <Typography.Text style={{ fontSize: '1.2em', color: '#fff' }}>
          Customer Login
        </Typography.Text>
      </div>
      <div
        style={{
          width: '60%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100vh',
          flexDirection: 'column',
        }}
      >
        <img
          src={rangoli}
          style={{
            width: 130,
            marginBottom: 100,
          }}
        />

        <Form
          onF
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Mobile"
            name="mobile"
            rules={[
              {
                required: true,
                message: 'Please enter your mobile number!',
              },
              {
                min: 10,
                max: 10,
                message: 'Please enter valid mobile number!',
              },
            ]}
          >
            <Input prefix={<MobileOutlined />} />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                min: 6,
                message: 'Password will be minimum 6 character',
              },
            ]}
          >
            <Input.Password prefix={<LockOutlined />} />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" loading={loading}>
              Login
            </Button>
          </Form.Item>
        </Form>
        {errorMessage && (
          <Space>
            {' '}
            <Alert message="Error!" description={errorMessage} type="error" />
          </Space>
        )}
      </div>
    </div>
  );
};
export default Login;
