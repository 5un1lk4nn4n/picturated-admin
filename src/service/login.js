import { URLS } from '../constants/urls.js';
import apiService from './api.js';

const loginService = new apiService(URLS.login);

export default loginService.login;
