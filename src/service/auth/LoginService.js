import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { BACKEND_DOMAIN, CURRENT_DOMAIN } from '../../constants/urls.js';
axios.defaults.baseURL = BACKEND_DOMAIN;
class LoginService {
  async login({ mobile, password }) {
    try {
      const response = await axios.post('/token/', {
        mobile,
        password,
      });
      console.log(response.status);
      if (response.status === 200) {
        const { access, refresh } = response.data;
        const decodedToken = jwt_decode(access);
        localStorage.setItem('token', access);
        localStorage.setItem('refreshToken', refresh);
        localStorage.setItem('company', decodedToken['company']);
        window.location.replace(CURRENT_DOMAIN);
      }
    } catch (err) {
      console.log({
        err,
      });
      if (err?.response?.status === 401) {
        throw new Error('Mobile or Password is invalid! Please try again.');
      }
      throw new Error(
        'Sorry, Cannot able to connect to server. Please try again later.'
      );
    }
  }
}

export default LoginService;
