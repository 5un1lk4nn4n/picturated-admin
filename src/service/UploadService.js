import { URLS } from '../constants/urls.js';
import apiService from './api.js';

const eventService = new apiService();

class EventService {
  async getMenu() {
    const response = await eventService.list(URLS.eventsMenu);
    return response;
  }
  async getEventDetail(id) {
    const response = await eventService.getDetail(URLS.eventsList, id);
    return response;
  }

  async getEventList(queryParams) {
    const response = await eventService.list(URLS.eventsList, queryParams);
    return response;
  }
  async createNewEvent(body) {
    const response = await eventService.create(URLS.eventsList, body);
    return response;
  }
}

export default EventService;
