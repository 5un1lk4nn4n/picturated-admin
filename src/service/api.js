import axios from 'axios';
import queryString from 'query-string';
import {BACKEND_DOMAIN, CURRENT_DOMAIN} from '../constants/urls.js';
import {jsToPythonRequestFormat, pythonToJsResponseFormat} from "../utils/stringConversion.js";

axios.defaults.baseURL = BACKEND_DOMAIN;

const protectedBackend = axios.create({
    baseURL: BACKEND_DOMAIN,
    headers: {
        'Content-Type': 'application/json',
    },
});

protectedBackend.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => Promise.reject(error)
);

// Add a response interceptor
protectedBackend.interceptors.response.use(
    (response) => response,
    async (error) => {
        const originalRequest = error.config;

        // If the error status is 401 and there is no originalRequest._retry flag,
        // it means the token has expired and we need to refresh it
        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;

            try {
                const refreshToken = localStorage.getItem('refreshToken');
                const response = await axios.post('/refresh-token/', {
                    refreshToken,
                });
                const {token} = response.data;

                localStorage.setItem('token', token);

                // Retry the original request with the new token
                originalRequest.headers.Authorization = `Bearer ${token}`;
                return axios(originalRequest);
            } catch (error) {
                // Handle refresh token error or redirect to login
                window.location.replace(`${CURRENT_DOMAIN}/login`);
            }
        }

        return Promise.reject(error);
    }
);

class apiService {
    async list(url, queryParams = null) {
        if (queryParams) {
            const stringified = queryString.stringify(queryParams);
            url = `${url}?${stringified}`;
        }
        const response = await protectedBackend.get(url);
        return response.data;
    }

    async getDetail(url, id) {
        const urlWithId = `${url}/${id}`;
        const response = await protectedBackend.get(urlWithId);
        return pythonToJsResponseFormat(response.data);
    }

    async create(url, body) {
        const payload = JSON.stringify(jsToPythonRequestFormat(body))
        const response = await protectedBackend.post(url, payload);
        return pythonToJsResponseFormat(response.data);
    }

    async update(url, body) {
        console.log(
            body,
            JSON.stringify(jsToPythonRequestFormat(body))
        )
        const payload = JSON.stringify(jsToPythonRequestFormat(body))
        const response = await protectedBackend.put(url, payload);
        return response.data;
    }
}

export default apiService;
