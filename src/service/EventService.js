import {URLS} from '../constants/urls.js';
import apiService from './api.js';

const eventService = new apiService();

class EventService {
    async getMenu() {
        return await eventService.list(URLS.eventsMenu);
    }

    async getEventDetail(id) {
        console.log('getEventDetail', id);
        return await eventService.getDetail(URLS.eventsList, id);
    }

    async getEventList(queryParams) {
        return await eventService.list(URLS.eventsList, queryParams);
    }

    async createNewEvent(body) {
        return await eventService.create(`${URLS.eventsList}/`, body);
    }

    async updateEvent(id, body) {
        return await eventService.update(`${URLS.eventsList}/${id}`, body);
    }
}

export default EventService;
