import { URLS } from '../constants/urls.js';
import apiService from './api.js';

const eventMenuService = new apiService(URLS.eventsMenu);

class eventDetailService {
  async getMenu() {
    const response = await eventMenuService.get();
    return response;
  }
  async getEventDetail(id) {
    const response = await eventMenuService.getDetail(id);
    return response;
  }
}

export default eventDetailService;
