import { MobileOutlined, WhatsAppOutlined } from '@ant-design/icons';
import { Image, QRCode, Typography } from 'antd';
import PropTypes from 'prop-types';
import logo from '../assets/white-logo-no-text.png';

export default function PosterTemplate({
  color,
  size,
  eventCode,
  whatsappMobile,
  mobile,
}) {
  return (
    <div
      style={{
        padding: 5,
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'start',
        }}
      >
        <div
          style={{
            width: '200px', //one
            textAlign: 'right',
          }}
        >
          <div
            style={{
              background: color,
              color: '#fff',
              fontSize: '2em', // two
              padding: 5,
            }}
          >
            SCAN
          </div>
          <div
            style={{
              fontSize: '1.1em', // three
              padding: 5,
            }}
          >
            To get your photos
          </div>
        </div>
        <div
          style={{
            background: color,
            color: '#fff',
            padding: 5,
          }}
        >
          <Image preview={false} src={logo} width={60} /> {/* four */}
        </div>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center',
          marginTop: 40,
          marginBottom: 40,
        }}
      >
        <QRCode
          size={size}
          color={color}
          errorLevel="H"
          value={eventCode}
          // icon="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"
        />
      </div>
      <div
        style={{
          height: 5,
          background: color,
          width: '80%',
          margin: '0px auto',
        }}
      />
      <div
        style={{
          padding: 10,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Typography.Text style={{ fontSize: '1.3em' }}>
          {/* five */}
          CONNECT WITH US!
        </Typography.Text>
        <Typography.Text style={{ fontSize: '1.1em' }}>
          <WhatsAppOutlined />
          {whatsappMobile}
        </Typography.Text>
        <Typography.Text style={{ fontSize: '1.1em' }}>
          <MobileOutlined /> {mobile}
        </Typography.Text>
      </div>
      <div
        style={{
          height: 3,
          background: color,
          width: '90%',
          margin: '0px auto',
        }}
      />
    </div>
  );
}

PosterTemplate.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
  eventCode: PropTypes.string,
  whatsappMobile: PropTypes.string,
  mobile: PropTypes.string,
};
