export default function PageNotFound() {
  return (
    <div
      style={{
        fontSize: '4em',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '98vh',
      }}
    >
      WHOOPS! Page Not Found
    </div>
  );
}
