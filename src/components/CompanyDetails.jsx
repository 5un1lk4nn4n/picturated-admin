import { Button, Col, Form, Input, Row, Select } from 'antd';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import EventService from '../service/EventService';
import Uploader from './Uploader';
const { Option } = Select;

const eventService = new EventService();
export default function CompanyDetail({ onCreateSubmit, eventData }) {
  const [event, setEvent] = useState(eventData);

  const onChange = (value, key) => {
    setEvent((prevState) => {
      return { ...prevState, [key]: value };
    });
  };

  const onEventDateChange = (value, key) => {
    const addOneday = dayjs(value[1]).add(1, 'day');
    const photoSharingDate = addOneday.format('YYYY-MM-DD');
    setEvent((prevState) => {
      return { ...prevState, [key]: value, photoSharingDate };
    });
  };

  const disabledPastDays = (current) =>
    current.isBefore(dayjs().subtract(1, 'day'));

  console.log({
    event,
    da: event.photoSharingDate,
  });
  return (
    <div
      style={{
        display: 'flex',
        // gap: 10,
        justifyContent: 'start',
      }}
    >
      <div
        style={{
          width: '100%',
          padding: 10,
          // border: '1px solid #e7e7ea',
          justifyContent: 'start',
        }}
      >
        <Form
          disabled
          layout="vertical"
          hideRequiredMark
          onFinish={() => {
            eventService.createNewEvent(event).then((response) => {
              console.log({
                response,
              });
              onCreateSubmit(response);
            });
          }}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Company Name"
                rules={[
                  {
                    required: true,
                    message: 'Please enter Company Name',
                  },
                ]}
              >
                <Input
                  value={event?.name}
                  placeholder="Eg. Qolphin Technologies"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name="Address" label="Full Address">
                <Input.TextArea
                  size="large"
                  rows={4}
                  placeholder="Please enter full address"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Pin Code"
                rules={[
                  {
                    required: true,
                    message: 'Please Pin Code',
                  },
                ]}
              >
                <Input
                  value={event?.name}
                  placeholder="Pincode"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Contact No"
                rules={[
                  {
                    required: true,
                    message: 'Please enter mobile number',
                  },
                ]}
              >
                <Input
                  value={event?.name}
                  placeholder="Mobile"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name="logo" label="Upload Logo (Optional)">
                <Uploader
                  onFinishUpload={(id) => {
                    onChange(id, 'poster');
                  }}
                  url={event?.posterUrl}
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <Button type="primary" htmlType="submit" size="large">
              Update
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

CompanyDetail.propTypes = {
  onCreateSubmit: PropTypes.func.isRequired,
  eventData: PropTypes.object,
};

CompanyDetail.defaultProps = {
  eventData: {},
};
