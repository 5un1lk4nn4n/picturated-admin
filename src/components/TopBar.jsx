import { FieldTimeOutlined, PoweroffOutlined } from '@ant-design/icons';
import { Avatar, Button, Tooltip, Typography } from 'antd';
import React from 'react';
import { useLocation } from 'wouter';
import logoSvg from '../assets/logo.svg';

const companyName = localStorage.getItem('company');

export default function Topbar() {
  const [location, setLocation] = useLocation();

  return (
    <div
      style={{
        display: 'flex',
        padding: 10,
        margin: 1,
        width: '98%',
        border: '1px solid #e7e7ea',
        justifyContent: 'space-between',
        gap: 15,
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'start',
          gap: 10,
          alignItems: 'center',
        }}
      >
        {/* <Image src={logo} preview={false} width={50} /> */}
        <img src={logoSvg} style={{ width: 50 }} alt="logo" />

        <Button
          href="/"
          type="primary"
          // style={{
          //   background: '#1B9C85',
          // }}
          icon={<FieldTimeOutlined />}
        >
          Current & Upcomming
        </Button>

        <Button href="/events-overview" type="ghost">
          Events Overview
        </Button>
        <Button href="/qolphin-settings" type="ghost">
          Qolphin Settings
        </Button>
        {/* <Button href="#" type="ghost">
          My Setting
        </Button> */}
      </div>
      <div
        style={{
          display: 'inherit',
          gap: 5,
          alignItems: 'center',
        }}
      >
        <div
          style={{
            display: 'inherit',
            gap: 5,
            border: '1px solid  #4c4c4c ',
            borderRadius: 15,
            padding: 5,
            alignItems: 'center',
          }}
        >
          <Typography.Text>{companyName.toUpperCase()}</Typography.Text>

          <Avatar
            // icon={<SmileOutlined/>}
            style={{ backgroundColor: '#4c4c4c' }}
          >
            {companyName ? companyName.charAt(0) : null}
          </Avatar>
        </div>

        <Tooltip title="Log out" color="#4c4c4c">
          <Button
            onClick={() => {
              localStorage.clear();
            }}
            href="/login"
            icon={<PoweroffOutlined />}
            style={{
              backgroundColor: '#ff4d4f',
              color: '#fff',
            }}
          ></Button>
        </Tooltip>
      </div>
    </div>
  );
}
