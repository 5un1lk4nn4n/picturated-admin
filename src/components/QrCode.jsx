import {Button, Checkbox, ColorPicker, Form, Input, Slider} from 'antd';
import React, {useState} from 'react';

import {PrinterOutlined} from '@ant-design/icons';

import ReactToPrint from 'react-to-print';
import PosterTemplate from './PosterTemplate';

const CheckboxGroup = Checkbox.Group;
const plainOptions = [
    'Event Name',
    'Company Name',
    'Company Logo',
    'Company Contact info',
];
const defaultCheckedList = ['Event Name'];
const QrCodeDesign = ({eventCode}) => {
    const [color, setColor] = useState('#254675');
    const [qrCode, setQrCode] = useState(eventCode);
    const [qrCodeSize, setQrCodeSize] = useState(200);
    const componentRef = React.useRef(null);
    const [checkedList, setCheckedList] = useState(defaultCheckedList);
    const onBeforeGetContentResolve = React.useRef(null);

    const [loading, setLoading] = React.useState(false);
    const [text, setText] = React.useState('old boring text');

    const handleAfterPrint = React.useCallback(() => {
        console.log('`onAfterPrint` called');
    }, []);

    const handleBeforePrint = React.useCallback(() => {
        console.log('`onBeforePrint` called');
    }, []);

    const handleOnBeforeGetContent = React.useCallback(() => {
        console.log('`onBeforeGetContent` called');
        setLoading(true);
        setText('Loading new text...');

        return new Promise((resolve) => {
            onBeforeGetContentResolve.current = resolve;

            setTimeout(() => {
                setLoading(false);
                setText('New, Updated Text!');
                resolve();
            }, 2000);
        });
    }, [setLoading, setText]);

    React.useEffect(() => {
        if (
            text === 'New, Updated Text!' &&
            typeof onBeforeGetContentResolve.current === 'function'
        ) {
            onBeforeGetContentResolve.current();
        }
    }, [onBeforeGetContentResolve.current, text]);

    const reactToPrintContent = React.useCallback(() => {
        return componentRef.current;
    }, [componentRef.current]);

    const reactToPrintTrigger = React.useCallback(() => {
        // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
        // to the root node of the returned component as it will be overwritten.

        // Bad: the `onClick` here will be overwritten by `react-to-print`
        // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

        // Good
        return (
            <Button type="primary" icon={<PrinterOutlined/>}>
                Print this Template
            </Button>
        );
    }, []);

    const onChange = (list) => {
        setCheckedList(list);
    };

    console.log({
        componentRef,
    });
    return (
        <div
            style={{
                display: 'flex',
                gap: 10,
                minHeight: '70vh',
            }}
        >
            <div
                style={{
                    width: '50%',
                    // background: 'yellow',
                    padding: 5,
                }}
            >
                <div
                    style={{
                        marginLeft: 50,
                        marginBottom: 50,
                        marginTop: 10,
                    }}
                >
                    <CheckboxGroup
                        options={plainOptions}
                        value={checkedList}
                        onChange={onChange}
                    />
                </div>

                <Form
                    labelCol={{
                        span: 4,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    layout="horizontal"
                    style={{
                        maxWidth: 600,
                    }}
                >
                    <Form.Item label="Event Name">
                        <Input
                            size="large"
                            style={{
                                marginBottom: 20,
                            }}
                        />
                    </Form.Item>
                    <Form.Item label="QR Code Size">
                        <Slider
                            max={300}
                            min={150}
                            defaultValue={200}
                            onChange={(val) => {
                                setQrCodeSize(val);
                            }}
                            // tooltip={{
                            //   open: true,
                            // }}
                        />
                    </Form.Item>
                    <Form.Item label="Choose Color">
                        <ColorPicker
                            value={color}
                            onChangeComplete={(color) => {
                                console.log(color);
                                setColor(color.toHexString());
                            }}
                        />
                    </Form.Item>
                    <Form.Item label="Footer Label">
                        <Input size="large"/>
                    </Form.Item>
                    <Form.Item label="Mobile">
                        <Input size="large"/>
                    </Form.Item>
                    <Form.Item label="Whatsapp">
                        <Input size="large"/>
                    </Form.Item>
                </Form>
                <ReactToPrint
                    content={reactToPrintContent}
                    documentTitle={`Event-${eventCode}-Code`}
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={handleOnBeforeGetContent}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                />
            </div>
            <div
                style={{
                    display: 'flex',
                    gap: 10,
                    justifyContent: 'start',
                    borderRadius: 15,
                    alignItems: 'center',
                    // background: 'tomato',
                    width: '50%',
                    border: '1px solid #4c4c4c',
                    flexDirection: 'column',
                    padding: 10,
                }}
            >
                <div
                    style={{
                        // background: 'tomato',
                        width: '100%',
                    }}
                >
                    {/* {loading && (
            <p className="indicator">onBeforeGetContent: Loading...</p>
          )} */}
                    <div
                        ref={componentRef}
                        style={{
                            height: '65vh',
                            overflow: 'auto',
                            // width: '70%'
                        }}
                    >
                        <PosterTemplate
                            color={color}
                            size={qrCodeSize}
                            mobile={'995549394'}
                            whatsappMobile={'9839847733'}
                            eventCode={'xSvd45gDWcff667'}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};
export default QrCodeDesign;
