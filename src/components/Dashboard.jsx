import {
  DatabaseOutlined,
  DownloadOutlined,
  EyeOutlined,
  FileImageOutlined,
  SmileOutlined,
  WhatsAppOutlined,
} from '@ant-design/icons';
import { Card, Col, Row, Statistic } from 'antd';
import React from 'react';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
];
const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sydney No. 1 Lake Park',
  },
];

export default function Dashboard({ data }) {
  const {
    totalImages,
    totalSharingRequest,
    storage,
    views,
    downloads,
    whatsappCredit,
  } = data;
  return (
    <>
      <Row gutter={16}>
        <Col span={4}>
          <Card>
            <Statistic
              title="Total Photos"
              value={totalImages}
              valueStyle={{
                color: '#008170',
              }}
              prefix={<FileImageOutlined />}
              suffix="#"
            />
          </Card>
        </Col>
        <Col span={4}>
          <Card>
            <Statistic
              title="Total Sharing"
              value={totalSharingRequest}
              valueStyle={{
                color: '#793FDF',
              }}
              prefix={<SmileOutlined />}
              suffix="#"
            />
          </Card>
        </Col>
        <Col span={4}>
          <Card>
            <Statistic
              title="Storage"
              value={storage}
              precision={2}
              valueStyle={{
                color: '#900C3F',
              }}
              prefix={<DatabaseOutlined />}
              suffix="GB"
            />
          </Card>
        </Col>
        <Col span={4}>
          <Card>
            <Statistic
              title="Views"
              value={views}
              valueStyle={{
                color: '#80558C',
              }}
              prefix={<EyeOutlined />}
              suffix="#"
            />
          </Card>
        </Col>
        <Col span={4}>
          <Card>
            <Statistic
              title="Downloads"
              value={downloads}
              valueStyle={{
                color: '#CD0404',
              }}
              prefix={<DownloadOutlined />}
              suffix="#"
            />
          </Card>
        </Col>
        <Col span={4}>
          <Card>
            <Statistic
              title="Whatsapp"
              value={whatsappCredit}
              valueStyle={{
                color: '#59CE8F',
              }}
              prefix={<WhatsAppOutlined />}
              suffix={<span style={{ fontSize: '.6em' }}>/100</span>}
            />
          </Card>
        </Col>
      </Row>
      {/* <Table
        bordered
        columns={columns}
        dataSource={data}
        pagination={false}
        size="small"
        title={() => 'Recent Sales-You made 265 sales this month.'}
        style={{
          width: '50%',
          marginTop: 20,
        }}
      /> */}
    </>
  );
}
