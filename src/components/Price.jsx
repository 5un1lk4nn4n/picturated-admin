import { Button, Card, Col, Row } from 'antd';
import Typography from 'antd/es/typography/Typography';
import React from 'react';
const Price = () => (
  <Row gutter={16}>
    <Col span={8}>
      <Card title="Basic" bordered={false}>
        Card content
      </Card>
    </Col>
    <Col span={8}>
      <Card title="Standard" bordered={false}>
        Card content
      </Card>
    </Col>
    <Col span={8}>
      <Card title="Solid" bordered={false}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Typography.Text>Unlimited Sharing</Typography.Text>
          <Typography.Text>Unlimited Sharing</Typography.Text>
          <Typography.Text>Unlimited Sharing</Typography.Text>
          <Typography.Text>Unlimited Sharing</Typography.Text>
          <Button>Upgrade</Button>
        </div>
      </Card>
    </Col>
  </Row>
);
export default Price;
