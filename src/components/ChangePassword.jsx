import { Button, Col, Form, Input, Row, Select } from 'antd';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import EventService from '../service/EventService';
const { Option } = Select;

const eventService = new EventService();
export default function ChangePassword({ onCreateSubmit, eventData }) {
  const [event, setEvent] = useState(eventData);

  const onChange = (value, key) => {
    setEvent((prevState) => {
      return { ...prevState, [key]: value };
    });
  };

  return (
    <div
      style={{
        display: 'flex',
        // gap: 10,
        justifyContent: 'start',
      }}
    >
      <div
        style={{
          width: '100%',
          padding: 10,
          // border: '1px solid #e7e7ea',
          justifyContent: 'start',
        }}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          onFinish={() => {
            eventService.createNewEvent(event).then((response) => {
              console.log({
                response,
              });
              onCreateSubmit(response);
            });
          }}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Old Password"
                rules={[
                  {
                    required: true,
                    message: 'Please enter old Password',
                  },
                ]}
              >
                <Input.Password
                  value={event?.name}
                  placeholder="Old Password"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
              <Form.Item
                name="name"
                label="New Password"
                rules={[
                  {
                    required: true,
                    message: 'Please enter new Password',
                  },
                ]}
              >
                <Input
                  value={event?.name}
                  placeholder="New Password"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
              <Form.Item
                name="name"
                label="New Password again"
                rules={[
                  {
                    required: true,
                    message: 'Please enter new passoward again',
                  },
                ]}
              >
                <Input
                  value={event?.name}
                  placeholder="Repeat new password again"
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <Button type="primary" htmlType="submit" size="large">
              Change
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

ChangePassword.propTypes = {
  onCreateSubmit: PropTypes.func.isRequired,
  eventData: PropTypes.object,
};

ChangePassword.defaultProps = {
  eventData: {},
};
