import { Button, Col, DatePicker, Form, Input, notification, Row } from 'antd';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import EventService from '../service/EventService';
import useEventDetailStore from '../stores/eventDetailStore.js';
import Uploader from './Uploader';

const eventService = new EventService();
export default function Settings({ onCreateSubmit, eventData }) {
  const event = useEventDetailStore((state) => state.event);

  const [form] = Form.useForm();
  const [eventObj, setEventObj] = useState(event);
  const [api, contextHolder] = notification.useNotification();

  const [isSaving, setIsSaving] = useState(false);
  console.log('zustand', {
    event,
    eventData,
    eventObj,
  });
  const openNotification = (type, title, description) => {
    api[type]({
      message: title,
      description: description,
      placement: 'bottomRight',
    });
  };

  useEffect(() => {
    const eventDate = eventObj?.startDate
      ? [dayjs(eventObj.startDate), dayjs(eventObj.endDate)]
      : null;
    const photoSharingEndDate = eventObj?.photoSharingEndDate
      ? dayjs(eventObj?.photoSharingEndDate)
      : null;

    form.setFieldsValue({
      qrCode: eventObj?.qrCode,
      name: eventObj?.name,
      eventDate,
      photoSharingEndDate: photoSharingEndDate,
    });
  }, [eventObj]);
  const onChange = (value, key) => {
    setEventObj((prevState) => {
      return { ...prevState, [key]: value };
    });
  };

  const onEventDateChange = (value) => {
    const [startDate, endDate] = value;
    const addOneday = dayjs(endDate).add(1, 'day');
    const photoSharingEndDate = addOneday.format('YYYY-MM-DD');
    setEventObj((prevState) => {
      return { ...prevState, startDate, endDate, photoSharingEndDate };
    });
  };

  const disabledPastDays = (current) =>
    current.isBefore(dayjs().subtract(1, 'day'));

  const creatOrUpdateEvent = () => {
    setIsSaving(true);
    if (!eventObj?.id) {
      eventService
        .createNewEvent(eventObj)
        .then((response) => {
          onChange(response['qrCode'], 'qrCode');
          // setEvent(response)
          onCreateSubmit(response);
          openNotification('success', 'Event Created!');
        })
        .then(() => setIsSaving(false));
    } else {
      eventService
        .updateEvent(eventObj.id, eventObj)
        .then((response) => {
          onCreateSubmit(response);

          openNotification('success', 'Event Updated!');
        })
        .then(() => setIsSaving(false));
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        // gap: 10,
        justifyContent: 'start',
      }}
    >
      {contextHolder}
      <div
        style={{
          width: '100%',
          padding: 10,
          // border: '1px solid #e7e7ea',
          justifyContent: 'start',
        }}
      >
        <Form form={form} layout="vertical" onFinish={creatOrUpdateEvent}>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="name"
                label="Event Name"
                rules={[
                  {
                    required: true,
                    message: 'Please enter Event Name',
                  },
                ]}
              >
                <Input
                  placeholder="Eg. Celebrating Meera’s Promotion!  "
                  size="large"
                  onChange={(e) => {
                    onChange(e.target.value, 'name');
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="qrCode"
                label="Event Code (Auto generated unique ID)"
              >
                <Input size="large" disabled />
                {/*<Typography.Text mark>{event?.qrCode}</Typography.Text>*/}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="eventDate"
                label="Event Date"
                rules={[
                  {
                    required: true,
                    message: 'Please choose the Start and End Date',
                  },
                ]}
              >
                <DatePicker.RangePicker
                  size="large"
                  style={{
                    width: '100%',
                  }}
                  disabledDate={disabledPastDays}
                  onChange={(date, dateString) => {
                    onEventDateChange(dateString, 'eventDate');
                  }}
                  getPopupContainer={(trigger) => trigger.parentElement}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="photoSharingEndDate"
                style={{
                  width: '100%',
                }}
                label="Photo Sharing last date (default 1 day)"
              >
                <DatePicker
                  style={{
                    width: '100%',
                  }}
                  onChange={(date, dateString) => {
                    onChange(dateString, 'photoSharingEndDate');
                  }}
                  size="large"
                  disabledDate={disabledPastDays}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item name="poster" label="Upload Poster (Optional)">
                <Uploader
                  onFinishUpload={(id) => {
                    console.log('Upload done', id);
                    onChange(id, 'poster');
                  }}
                  url={eventObj?.posterUrl}
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              size="large"
              loading={isSaving}
            >
              Proceed
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

Settings.propTypes = {
  onCreateSubmit: PropTypes.func.isRequired,
  // eventData: PropTypes.object,
};

// Settings.defaultProps = {
//     eventData: {},
// };
