import { Button, Divider, Form, Input, Space, Tag } from 'antd';
import React from 'react';
const Collabarators = () => (
  <>
    <Space size={[0, 'small']} wrap>
      <Tag
        bordered={false}
        closable
        color="orange"
        style={{
          padding: 5,
        }}
      >
        Sunil (9995678194)
      </Tag>
      <Tag
        bordered={false}
        closable
        color="orange"
        style={{
          padding: 5,
        }}
      >
        Sunil (9995678194)
      </Tag>
    </Space>
    <Divider />
    <Space size={[0, 'small']} wrap>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={() => {}}
        onFinishFailed={() => {}}
        autoComplete="off"
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mobile"
          name="mobile"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Space>
  </>
);
export default Collabarators;
