import { CameraOutlined } from '@ant-design/icons';
import { Image, Result, Typography } from 'antd';
import React from 'react';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import pic1 from '../assets/sample/1.jpg';
import pic2 from '../assets/sample/2.jpg';
import pic4 from '../assets/sample/3.jpg';
import pic3 from '../assets/sample/4.jpg';
import Uploader from './Uploader';

const Gallery = () => {
  const [gallery, setGallery] = React.useState([]);
  const imageGallery = new Array(10).fill([pic1, pic2, pic3, pic4]).flat();

  const getGallery = (images) => setGallery(images);

  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <div
        style={{
          // background: 'yellow',
          border: '1px solid #e7e7ea',
          width: '75%',
          height: '70vh',
          // borderRadius: 15,
          overflow: 'auto',
        }}
      >
        <div
          style={{
            height: '100%',
          }}
        >
          {gallery.length === 0 ? (
            <div
              style={{
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Result
                icon={<CameraOutlined style={{ color: '#89E234' }} />}
                title="Start uploading your images!"
                // extra={<Button type="primary">Next</Button>}
              />{' '}
            </div>
          ) : (
            <ResponsiveMasonry
              columnsCountBreakPoints={{ 300: 2, 500: 3, 700: 4, 900: 5 }}
            >
              <Masonry>
                {imageGallery.map((image, index) => {
                  return (
                    <div key={index} style={{ margin: 5, borderRadius: 15 }}>
                      <Image
                        src={image}
                        style={{
                          borderRadius: 10,
                        }}
                      />
                    </div>
                  );
                })}
              </Masonry>
            </ResponsiveMasonry>
          )}
        </div>
      </div>
      <div
        style={{
          // width: '30%',
          paddingLeft: '5%',
          paddingTop: '3%',
          display: 'flex',
          flexDirection: 'column',
          // margin: 'auto',
          // justifyContent: 'center',
          alignItems: 'center',
          // background: 'yellow',
          gap: 5,
        }}
      >
        <Typography.Text>Upload Your Images here</Typography.Text>
        <Typography.Text type="secondary">
          Only images up to 10MB per image
        </Typography.Text>
        <div
          style={{
            marginTop: 30,
          }}
        >
          <Uploader />
        </div>
      </div>
    </div>
  );
};
export default Gallery;
